from data_loader import config, load

data_resource = {
  'path': '100kb.csv',
  'ckan_resource_id': '5d1af90f-cc57-4271-a990-1116dd1a400e',
  "schema": {
    "fields": [
      {
        "name": "first_name",
        "type": "string"
      },
      {
        "name": "last_name",
        "type": "string"
      },
      {
        "name": "email",
        "type": "string"
      },
      {
        "name": "gender",
        "type": "string"
      },
      {
        "name": "ip_address",
        "type": "string"
      },
      {
        "name": "date",
        "type": "string"
      }
    ]
  }
}

load.load_csv(data_resource, config.get_config(), config.get_connection())
