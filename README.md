Data loader for CKAN next-gen. Loads CSV (and similar) data into DataStore. Designed as a replacement for other loaders.

## Install requirements

You will need python 3.x to run the code.

```
pip install -r requirements.txt
```

## Usage

### Environment Variables

You will need following env variables. You can either export or keep them in `.env` file (in root directory)

- `CKAN_DATASTORE_WRITE_URL` - A valid Postgres database [connection URI](https://www.postgresql.org/docs/9.3/libpq-connect.html#AEN39692)  
- `CKAN_SYSADMIN_API_KEY` - API key for system administrator on CKAN instance
- `CKAN_SITE_URL` - Website URL fro ckan instance

### Run the code

```
python main.py
```

## Running the Tests

### Install dev requirements

```
pip install -r requirements.dev.txt
```

To run tests you will need to have running postgres database. and it's connection URI. Together with other env variables defined. Create `.env` like example below.

```
CKAN_DATASTORE_WRITE_URL=postgresql://ckan:123456@localhost/datastore
CKAN_SYSADMIN_API_KEY=CKaa-sYSadMin-api-key
CKAN_SITE_URL=https://demo.ckan.org
```

Run tests
```
make test
```
