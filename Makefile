.PHONY: test

test:
	python -m pytest tests -sv --cov=data_loader && pylama data_loader
